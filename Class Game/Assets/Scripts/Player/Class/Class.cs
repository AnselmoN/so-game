using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum WeaponWeight
{
    NONE,
    DAGGERS,
    LIGHTWEAPON,
    NORMALWEAPON,
    HEAVYWEAPON,
    STAFF
}

public abstract class Class : ScriptableObject
{
    public WeaponWeight weaponType;
    public Vector2 speed;
    public int health;
    public float mana;
    public Color color;

    public abstract void SpecialPower();
}


