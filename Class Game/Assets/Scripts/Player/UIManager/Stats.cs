using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Stats : MonoBehaviour
{
    private TextMeshProUGUI text;
    public PlayerModel playerModel;

    private string damage;
    private string speed;
    private string health;
    private string mana;
    private string durability;
    private string weaponType;

    private void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
        text.text = "";
    }

    private void Update()
    {
        if(playerModel.myClass != null)
        {
            speed = playerModel.myClass.speed.x.ToString();
            health = playerModel.myClass.health.ToString();
            mana = playerModel.myClass.mana.ToString();
        }
        else
        {
            speed = "0";
            health = "0";
            mana = "0";
        }
        if(playerModel.myWeapon != null)
        {
            weaponType = playerModel.myWeapon.name;
            damage = playerModel.myWeapon.damage.ToString();
            durability = playerModel.myWeapon.durability.ToString();
        }
        else
        {
            weaponType = "None";
            damage = "0";
            durability = "0";
        }

        text.text = "Player Stats:" +
                  "\nHealth = " + health +
                  "\nSpeed = " + speed +
                  "\nMana = " + mana +

                "\n\nWeapone Stats: " +
                  "\nWeaponType = " + weaponType +
                  "\nDamage = " + damage +
                  "\nDurability = " + durability;

    }
}
