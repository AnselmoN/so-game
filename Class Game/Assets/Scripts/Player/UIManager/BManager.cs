using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BManager : MonoBehaviour
{
    public PlayerModel target;
    public Class myClass;
    public Weapon myWeapon;

    public void Onclick()
    {
        target.myClass = myClass;

        if (target.myWeapon != null)
        {
            if (!IsAbleToUseWeapon(target.myWeapon.weight, target.myClass.weaponType))
            {
                target.myWeapon = null;
            }
        }
    }

    public void WeaponChange()
    {
        if (target.myClass != null)
        {
            if (IsAbleToUseWeapon(myWeapon.weight, target.myClass.weaponType))
            {
                target.myWeapon = myWeapon;
            }
        }
    }

    private bool IsAbleToUseWeapon(WeaponWeight target, WeaponWeight myClassW)
    {
            return (target == myClassW || 
                target == WeaponWeight.LIGHTWEAPON);
    }
}
