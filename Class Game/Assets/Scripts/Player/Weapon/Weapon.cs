using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Create Weapon/Character Weapon", fileName = "Weapon")]
public class Weapon : ScriptableObject
{
    public string typeName;
    public uint damage;
    public uint durability;
    public WeaponWeight weight;
}
