using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWalk : MonoBehaviour
{
    [SerializeField]
    private float maxSpeed;
    [SerializeField]
    private float maxDelta;
    PlayerJump jump;
    private Rigidbody2D rb2d;
    private PlayerController controller;
    private float orientation;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        controller = GetComponent<PlayerController>();
        jump = GetComponent<PlayerJump>();
    }

    // Update is called once per frame
    void Update()
    {
        if (jump.canJump)
        {
            orientation = controller.inputController.GetHorizontalInput();
        }
    }

    private void FixedUpdate()
    {
        var velocity = rb2d.velocity;
        velocity.x = Mathf.MoveTowards(velocity.x, orientation * maxSpeed, maxDelta);
        rb2d.velocity = velocity;
    }
}
