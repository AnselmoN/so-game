using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
    [SerializeField]
    private Vector2 jumpForce;
    private Rigidbody2D rb2d;
    private PlayerController controller;
    public bool canJump;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        controller = GetComponent<PlayerController>();
    }

    void Update()
    {
        if(controller.inputController.GetJumpInput() && canJump)
        {
            canJump = false;
            rb2d.AddForce(jumpForce);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "Ground")
        {
            canJump = true;
        }
    }
}
