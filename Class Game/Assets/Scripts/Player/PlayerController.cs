using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class PlayerController : MonoBehaviour
{
    public InputController inputController;
    public PlayerModel playerModel;
}


[CustomEditor(typeof(PlayerController))]
public class SaveAndLoad : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        PlayerController saveUI = (PlayerController)target;
        if (GUILayout.Button("Serialize to Binary Data"))
        {
            SaveData.SaveBinary(saveUI.playerModel);
        }

        if (GUILayout.Button("Load Binary Data"))
        {
            saveUI.playerModel = SaveData.LoadBinary();
        }
    }

}

