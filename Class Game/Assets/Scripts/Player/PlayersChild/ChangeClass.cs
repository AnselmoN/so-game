using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeClass : MonoBehaviour
{
    private PlayerModel parentsClass;
    private SpriteRenderer sprite;
    void Start()
    {
        parentsClass = GetComponentInParent<PlayerModel>();
        sprite = GetComponent<SpriteRenderer>();
        sprite.color = Color.white;
    }

    // Update is called once per frame
    void Update()
    {
        if(parentsClass.myClass != null && !IsMyColor())
        {
            sprite.color = parentsClass.myClass.color;
        }
    }

    private bool IsMyColor()
    {
        return parentsClass.myClass.color == sprite.color;
    }
}
