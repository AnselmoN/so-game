using System.IO;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using UnityEngine.Playables;


public class SaveData
{
    public static string directory = "/SavedData/";
    public static string fileName = "CharacterData.txt";

    public static void SaveBinary(PlayerModel playerData)
    {
        string dir = Application.persistentDataPath + directory;

        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }

        //Binary
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(dir + fileName);
        bf.Serialize(file, playerData);
        file.Close();

        Debug.Log("Binary File Created in " + dir);
    }

    public static PlayerModel LoadBinary()
    {
        string fullPath = Application.persistentDataPath + directory + fileName;
        PlayerModel playerData = new PlayerModel();

        if (File.Exists(fullPath))
        {
            try
            {
                //Binary
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(fullPath, FileMode.Open);
                playerData = (PlayerModel)bf.Deserialize(file);
            }
            catch
            {
                Debug.Log("Error for Loading an Unchaged File");
            }
        }
        else
        {
            Debug.Log("Save does not exist");
        }

        return playerData;
    }


}
